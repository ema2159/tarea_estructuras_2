// Ejercicio 3. Tarea 2.
// Dunia Barhona & Emmanuel Bustos

#include <stdio.h>
#include <mpi.h>
#include <math.h>
#include <string.h>

/* Función que determina si un número dado es primo o no
 * Imprime primos en un archivo de texto
 * 
 * Recibe
 * id: identificador de cada procesador
 * numprocs: cantidad de procesadores disponibles
 * d: rango
 * f: archivo donde se imprimen los números primos
 * 
 * No devuelve nada
 */
void getPrime_F (int id, int numprocs, int d, FILE* f)
{
	_Bool primo;
	int i, j, n, fin;
	int start = id*d;
	
	for (i=0; i<d; i++)
	{
		primo =1;
		n = start+i;
		fin= sqrt(n)+1;
		
		///determinar si es primo
		if (n>1)
		{
			for (j=2; j<fin; j++)
			{	
				if (n%j == 0)
				{
					primo =0;	//no es primo
					j= fin+1;	//sale del for
				}	
			}
			if (primo==1)
			{		
				fprintf(f,"%d\n", n);	//el num es primo
			}
		}
		///	
	}
	return;
}

//// main ////
int main(int argc, char *argv[])
{
	double starttime, endtime;
	int ierr, id, numprocs, d, r;
	
	ierr= MPI_Init(&argc, &argv);					//Iniciar
	ierr= MPI_Comm_rank(MPI_COMM_WORLD, &id);		//asignar ID a cada procesador
	ierr= MPI_Comm_size(MPI_COMM_WORLD, &numprocs);	//obtener número de pocesadores que están corriendo
	
	//rango de números que debe revisar cada procesador
	d= 7368787/numprocs;
	r= 7368787%numprocs;
	
	if (r != 0)
	{
		d +=1;
	}
	
	if (id==0)
	{
		printf("\nCalculando los primeros 500 000 números primos de manera paralela (%d procesadores)...\n", numprocs);
	}
	
	FILE* f = fopen("Primos_Paralelo.txt","a+"); //abrir archivo
	
	starttime = MPI_Wtime();		//para medir tiempo	
	getPrime_F (id, numprocs, d, f);//obtener números primos	
	endtime   = MPI_Wtime();		//para medir tiempo	
	
	fclose(f);	//cerrar archivo
	
	if (id==0)
	{
		printf("Listo! ver archivo 'Primos_Paralelo.txt'\n\n");
	}
	
	printf("P%d time: %f seconds\n",id, endtime-starttime);
	ierr = MPI_Finalize();	//Finalizar
	
	return 0;
}	
