# TAREA 2
## IE0521 Estructuras de computadoras digitales II
### Ejercicio 3

Programas en C para calcular los primeros 500 000 números primos.

+ primos.c (secuencial)
+ primosMPI.c (paralelo)

Los archivos fuente y el Makefile se encuentran en la carpeta 'PrimeNumbers'. En la misma se generan los archivos con los números primos.

Para ejecutar el código se cuenta con los siguientes TARGETS en el makefile:

+ make:			compila y ejecuta la versión secuencial seguida de la implementación en paralelo.
+ make comp:	compila ambos programas
+ make runS:	ejecuta programa secuencial
+ make runP:	ejecuta programa Paralelo
+ make clean:	borra archivos ejecutables
+ make cleanAll:borra archivos ejecutables y los generados por los programas
