#include <stdio.h>
#include <math.h>
#include <time.h>

int main(int argc, char *argv[])
{
	_Bool primo; //determinar si num es primo o no
	int i, n =2, fin=2, cont =0, lim=500000;
    double seconds;
    
    printf("\nCalculando los primeros 500 000 números primos de manera secuencial...\n");
    
    FILE* f = fopen("Primos_Secuencial.txt","w+");	//abrir archivo
    
    clock_t begin = clock();	//para medir tiempo	
    
	while (cont<lim)
	{
		primo =1;	//booleano para indicar si es primo o no
		fin= sqrt(n)+1;
		
		///determinar si num tiene más de un divisor
		if (n>1)
		{
			for (i =2; i <fin; i++)
			{	
				if (n %i ==0)
				{
					primo =0;	//no es primo
					i= fin+1;	//sale del for
				}
			}
			if (primo==1)
			{	
				fprintf(f,"%d\n", n);	//escribir número primo encontrado
				cont ++;				//es primo
				//printf("%i)  %i\n", cont, n);
			}
			n++; //incremento del número	
		}
		///
	}
	clock_t end = clock();	//para medir tiempo	
	
	double runtime = (double)(end - begin)/ CLOCKS_PER_SEC;
	
	fprintf(f,"\nPrimeros %d números primos - Secuencial\n", lim);
	fclose(f);	//cerrar archivo
	printf("Listo! ver archivo 'Primos_Secuencial.txt'\nTiempo: %lf seconds\n\n", runtime);
	
	return 0;
}
