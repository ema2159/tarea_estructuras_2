/*
 * TSP.c
 * 
 * Copyright 2018 Emanuel Bustos <emanuel@debian>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "functions.c"

//Defines
#define MASTER 0 
#define FROM_WORKER 2    
#define SEND_ROW 1     
#define SEND_VEC 2
#define RECEIVE_RES 3
#define San_Jose 0
#define Limon 1
#define San_Francisco 2
#define Alajuela 3
#define Liberia 4
#define Paraiso 5
#define Puntarenas 6
#define San_Isidro 7

int main(int argc, char **argv)
{	//Variables de interés general
	int initial_node = strtol(argv[1], NULL, 10);
	int cities=8;
	int rank, size, dest;
	int** mat = (int**)malloc(cities * sizeof(int*));
		for (int i=0;i<cities;i++){
			mat[i] = (int*)malloc(cities * sizeof(int));
		}
    int* nodes = (int*)malloc(cities *sizeof(int));
    //MPI initialization
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	//Programa	
		
	mat[0] = (int[8]){0, 115, 8, 17, 167, 26, 83, 75};   
	mat[1] = (int[8]){115, 0, 120, 129, 272, 92, 197, 100};   
	mat[2] = (int[8]){8, 120, 0, 9, 160, 34, 78, 83};   
	mat[3] = (int[8]){17, 129, 9, 0, 151, 43, 69, 91};   
	mat[4] = (int[8]){167, 272, 160, 151, 0, 193, 98, 236};   
	mat[5] = (int[8]){26, 92, 34, 43, 193, 0, 108, 55};   
	mat[6] = (int[8]){83, 197, 78, 69, 98, 108, 0, 141};   
	mat[7] = (int[8]){75, 100, 83, 91, 236, 55, 141, 0};  
		
	MPI_Status status;
	//Se arregla la variable nodes para ser enviada a los otros procesos
	if(rank == MASTER){
		 if(initial_node>=cities){
			printf("ERROR: NODO INICIAL INCORRECTO \n");
			return 0;
		}
		for (int i = 0; i < cities; i++){
			nodes[i] = 0;
		}
		nodes[0] = initial_node;
		nodes = set_vec(nodes, cities, 0);
	}
	
	MPI_Bcast(nodes, cities, MPI_INT, MASTER, MPI_COMM_WORLD);
	
	//Cada core toma un nodo en el grafo del viaje, habiendo tomado 
	//previamente un nodo de partida, por lo que se subdivide el 
	//problema  
	if(rank<=cities-1 && rank>MASTER){
		int ranknodes = (cities-1)/(size-1);
		if(size>cities){
			ranknodes=1;
		}
		int rowoffset = ranknodes*(rank-1);
		if((cities-1)%(size-1)!=0 && rank==size-1){
			ranknodes=(cities-1)%(size-1)+((cities-1)/(size-1));
		}
		int* subresult = (int*)malloc(cities * sizeof(int));
		int* result = (int*)malloc(cities * sizeof(int));
		int local_min=INFINITY;
		int temp_min;
		for(int i = 0; i<cities; i++){
			subresult[i] = nodes[i];	
		}
		for(int i=0; i<ranknodes; i++){
			subresult[1]=nodes[rowoffset+i+1];
			subresult = set_vec(subresult, cities, 1);
			tsp(mat, subresult, 1, cities, &subresult);
			temp_min = costcalc(mat, subresult, cities);
			if(temp_min<local_min){
				local_min = temp_min;
				for(int i = 0; i<cities; i++){
					result[i] = subresult[i];
				}
			}
		}
		MPI_Send(&result[0], cities, MPI_INT, MASTER, RECEIVE_RES, MPI_COMM_WORLD);
	}
	
	//El core master recolecta los mínimos locales encontrados en el 
	//mínimo camino encontrado por cada core con el fin de compararlos
	//y encontrar el mínimo global
	if(rank == MASTER){
		int* recvresult = (int*)malloc(cities * sizeof(int));
		int* finalresult = (int*)malloc(cities * sizeof(int));
		int global_min = INFINITY;
		for(int sender=1; sender<size; sender++){
			if(sender<cities){
				MPI_Recv(&recvresult[0], cities, MPI_INT, sender, RECEIVE_RES, MPI_COMM_WORLD, &status);
				if(costcalc(mat, recvresult, cities)<global_min){
					global_min = costcalc(mat, recvresult, cities);
					for(int i = 0; i<cities; i++){
						finalresult[i] = recvresult[i];	
					}
				}
			}
		}
		
		printf("Cost Matrix: \n");
		printmat(mat, cities, cities);
		printf("Shortest Path: \n");
		printvec(finalresult, cities);
		for (int i = 0; i < cities; i++){
			if(finalresult[i] == San_Jose){printf("San José -> ");}
			else if(finalresult[i] == Limon){printf("Limon -> ");}
			else if(finalresult[i] == San_Isidro){printf("San Isidro -> ");}
			else if(finalresult[i] == San_Francisco){printf("San Francisco -> ");}
			else if(finalresult[i] == Alajuela){printf("Alajuela -> " );}
			else if(finalresult[i] == Liberia){printf("Liberia -> ");}
			else if(finalresult[i] == Paraiso){printf("Paraíso -> ");}
			else if(finalresult[i] == Puntarenas){printf("Puntarenas -> ");}
		}
		if(finalresult[0] == San_Jose){printf("San José \n ");}
		else if(finalresult[0] == Limon){printf("Limon \n");}
		else if(finalresult[0] == San_Isidro){printf("San Isidro \n ");}
		else if(finalresult[0] == San_Francisco){printf("San Francisco \n ");}
		else if(finalresult[0] == Alajuela){printf("Alajuela \n" );}
		else if(finalresult[0] == Liberia){printf("Liberia \n ");}
		else if(finalresult[0] == Paraiso){printf("Paraíso \n ");}
		else if(finalresult[0] == Puntarenas){printf("Puntarenas \n ");}
		printf("Cost: %d \n", costcalc(mat, finalresult, cities));
	}
	
	MPI_Finalize();
	
	return 0;		
}
