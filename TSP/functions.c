//#include <mpi.h>
#define INFINITY 100000 //costo mínimo inicial
#define randsize 5

//Matrix functions
void freemat(int** mat, int matrows) {
	for (int i=0;i<matrows;i++){
		free(mat[i]);
	}
	free(mat);
}

//Imprimir matriz
void printmat(int** mat, int matrows, int matcols) {
	for (int i=0;i < matrows;i++){
		for (int j = 0; j < matcols; j++){
			printf("%d\t", mat[i][j]);
		}
	printf("\n");
	}
}

//Inicializar matriz en valores aleatorios
void matrandomize(int** mat, int matrows, int matcols) {
	for (int i=0;i < matrows;i++){
		for (int j = 0; j < matcols; j++){
			mat[i][j] = rand()%randsize;
		}
	printf("\n");
	}
}

//Vector Functions
//Imprimir vector
void printvec(int* mat, int vecsize) {
	for (int i = 0; i < vecsize; i++){
		printf("%d\t", mat[i]);
	}
	printf("\n");
}

//Inicializar vector en valores aleatorios
void vecrandomize(int* mat, int vecsize) {
	for (int i = 0; i < vecsize; i++){
		mat[i] = rand()%randsize;
	}
	printf("\n");
}

//Matrix x Vector
int* vecXmat(int** mat, int*vec, int matrows, int matcols){
	int* vecresult = (int*)malloc(matcols * sizeof(int));
	for (int i = 0; i < matcols ; i++){
			vecresult[i]+=0;
		}
	for (int j = 0; j < matrows; j++){
		for (int i = 0; i < matcols ; i++){
			vecresult[j]+=vec[i]*mat[i][j];
		}
	}
	return vecresult;
}

int* set_vec(int* vector, int size, int initial){
	int k = 0;
	for(int i = initial+1; i < size; i++){
		for (int z = 0; z < i; z++){		
			for(int j = 0; j < i; j++){
				if(k == vector[j]){
					k++;
				}
		}
		vector[i] = k;
		}
		k++;
	}
	return vector;	
}



//Cálculo del costo de un camino dada una determinada matriz de costos
int costcalc(int** mat, int* vec, int size){
	int cost = 0;
	for(int i = 0; i < size-1; i++){
		cost+=mat[vec[i]][vec[i+1]];
	}
	cost+=mat[vec[size-1]][vec[0]];
	return cost;
}

//Este algoritmo resuelve el problema del viajante (TSP) mediante fuerza
//bruta haciendo uso de recursividad. Este funciona declarando un camino
//arbitrario en un arreglo en el cual se considera la posición 0 del 
//arreglo como el nodo de inicio y a partir de esto empezar a pivotear
//las demás posiciones hasta encontrar un mejor camino y continuando con
//esto hasta cubrir todos los caminos posibles determinando la mejor ruta
int tsp(int **c, int* tour, int start, int cities, int** final_tour){ 
	int* temp = (int*)malloc(cities *sizeof(int)); //Arreglo temporal para pivotear. 
	int* mintour = (int*)malloc(cities *sizeof(int)); //Arreglo con el camino más corto hasta el momento  
	int mincost; //Costo mínimo actual
	int ccost; //costo actual
	//Condición de parada de la función recursiva
	if (start == cities - 2){
		return c[tour[cities-2]][tour[cities-1]] + c[tour[cities-1]][tour[0]];
	} 
	//Pivotear desde la posición start del arreglo inicial, por ejemplo
	//si se empieza con el camino 01234567 y se toma la posición inicial
	//como la posición [0], se pivotean los números siguientes buscando
	//la mejor combinación (02134567, 03214567, 04231567...)
	mincost = INFINITY;
	for (int i = start+1; i<cities; i++){ 
		for (int j=0; j<cities; j++){
			temp[j] = tour[j];
		}
		temp[start+1] = tour[i];
		temp[i] = tour[start+1];
		 
		//Condición que compara el costo actual con el costo mínimo 
		//encontrado hasta el momento para determinar si es o no un
		//mejor camino. Nótese la recursividad.
		ccost = tsp(c, temp, start+1, cities, final_tour);
		if (c[tour[start]][tour[i]] + ccost  < mincost) {
			mincost = c[tour[start]][tour[i]] + ccost;
			for (int k=0; k<cities; k++){
				mintour[k] = temp[k];
			}
		}
	}
	 
	//Asignar al arreglo del camino el menor camino encontrado hasta el 
	//momento
	for (int i=0; i<cities; i++){
		tour[i] = mintour[i];
	} 
	//Pasar por referencia el camino ya que esta función posee dos 
	//salidas, el costo mínimo y el camino más corto.
	*final_tour = tour;
	return mincost;
}
