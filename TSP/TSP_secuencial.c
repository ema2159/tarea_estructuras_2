
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "functions.c"

//Defines
#define MASTER 0 
#define FROM_WORKER 2    
#define SEND_ROW 1     
#define SEND_VEC 2
#define RECEIVE_RES 3
#define San_Jose 0
#define Limon 1
#define San_Francisco 2
#define Alajuela 3
#define Liberia 4
#define Paraiso 5
#define Puntarenas 6
#define San_Isidro 7

int main(int argc, char **argv)
{	//Variables de interés general
	int initial_node = strtol(argv[1], NULL, 10);
	int cities=8;
	int** mat = (int**)malloc(cities * sizeof(int*));
		for (int i=0;i<cities;i++){
			mat[i] = (int*)malloc(cities * sizeof(int));
		}
	int* nodes = (int*)malloc(cities * sizeof(int));
		
	mat[0] = (int[8]){0, 115, 8, 17, 167, 26, 83, 75};   
	mat[1] = (int[8]){115, 0, 120, 129, 272, 92, 197, 100};   
	mat[2] = (int[8]){8, 120, 0, 9, 160, 34, 78, 83};   
	mat[3] = (int[8]){17, 129, 9, 0, 151, 43, 69, 91};   
	mat[4] = (int[8]){167, 272, 160, 151, 0, 193, 98, 236};   
	mat[5] = (int[8]){26, 92, 34, 43, 193, 0, 108, 55};   
	mat[6] = (int[8]){83, 197, 78, 69, 98, 108, 0, 141};   
	mat[7] = (int[8]){75, 100, 83, 91, 236, 55, 141, 0};  
		
	for (int i = 0; i < cities; i++){
		nodes[i] = 0;
	}
	
	nodes[0] = initial_node;
	nodes = set_vec(nodes, cities, 0);
	tsp(mat, nodes, 0, cities, &nodes);
	printf("Cost Matrix: \n");
	printmat(mat, cities, cities);
	printf("Shortest Path: \n");
	printvec(nodes, cities);
	for (int i = 0; i < cities; i++){
		if(nodes[i] == San_Jose){printf("San José -> ");}
		else if(nodes[i] == Limon){printf("Limon -> ");}
		else if(nodes[i] == San_Isidro){printf("San Isidro -> ");}
		else if(nodes[i] == San_Francisco){printf("San Francisco -> ");}
		else if(nodes[i] == Alajuela){printf("Alajuela -> " );}
		else if(nodes[i] == Liberia){printf("Liberia -> ");}
		else if(nodes[i] == Paraiso){printf("Paraíso -> ");}
		else if(nodes[i] == Puntarenas){printf("Puntarenas -> ");}
	}
	if(nodes[0] == San_Jose){printf("San José \n ");}
	else if(nodes[0] == Limon){printf("Limon \n");}
	else if(nodes[0] == San_Isidro){printf("San Isidro \n ");}
	else if(nodes[0] == San_Francisco){printf("San Francisco \n ");}
	else if(nodes[0] == Alajuela){printf("Alajuela \n" );}
	else if(nodes[0] == Liberia){printf("Liberia \n ");}
	else if(nodes[0] == Paraiso){printf("Paraíso \n ");}
	else if(nodes[0] == Puntarenas){printf("Puntarenas \n ");}
	
	printf("Cost: %d \n", costcalc(mat, nodes, cities));
	
}
