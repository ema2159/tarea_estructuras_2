# TAREA 2
## IE0521 Estructuras de computadoras digitales II
### Ejercicio 3

Programas en C que resuleve el problema del viajante para una matriz
de costos dada.

+ TSP.c
+ TSP_secuencial.c

Para ejecutar el programa, utilizar los siguientes comandos en la carpeta:

+ make sequential INITIAL=x, para ejecutar la versión secuencial
+ make parallel CORES=y INITIAL=x, para ejecutar la versión paralela

"x"" es la ciudad inicial que se desea para el recorrido y "y" es el 
número de cores deseados para la ejecución paralela.
