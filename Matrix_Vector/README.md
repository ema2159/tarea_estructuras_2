# TAREA 2
## IE0521 Estructuras de computadoras digitales II
### Ejercicio 3

Programas en C que multiplica una matriz por un vector columna.

+ Matvec.c
+ Matvec_secuencial.c

Para ejecutar el programa, utilizar los siguientes comandos en la carpeta:

+ make sequential ROWS=x, para ejecutar la versión secuencial
+ make parallel CORES=y ROWS=x, para ejecutar la versión paralela

"x" corresponde a las dimensiones de la matriz y el vector y "y" es el 
número de cores deseados para la ejecución paralela.
