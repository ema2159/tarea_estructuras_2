/*
 * Matvec.c
 * 
 * Copyright 2018 Emanuel Bustos <emanuel@debian>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "functions.c"

MPI_Status status;

//Defines
#define MASTER 0 
#define FROM_WORKER 2    
#define SEND_ROW 1     
#define SEND_VEC 2
#define RECEIVE_RES 3

int main(int argc, char **argv)
{	//Random number generator
	srand(time(NULL)); 
	//Variables setting
	int matrows = strtol(argv[1], NULL, 10);
	int col = matrows;
	int rank, size, dest;
	int* vec = (int*)malloc(matrows * sizeof(int));
	int* row = (int*)malloc(matrows * sizeof(int));
	int* vecresult = (int*)malloc(matrows * sizeof(int));
	int** mat = (int**)malloc(matrows * sizeof(int*));
	for (int i=0;i<matrows;i++){
		mat[i] = (int*)malloc(col * sizeof(int));
	}
	int** mat2;
	//MPI initialization
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	//Program
	int rankrows = matrows/(size-1);
	if(size>matrows){
		rankrows=1;
	}
	//Proceso que llena con valores aleatorios el vector y la matriz 
	if (rank == MASTER){
		matrandomize(mat, matrows, col);
		vecrandomize(vec, matrows);	
	}
	MPI_Status status;
	
	//Master envía la variable vec a todos los procesos
	MPI_Bcast(&vec[0], col, MPI_INT, MASTER, MPI_COMM_WORLD);
	//Master asigna filas a los otros cores de manera que todos los 
	//trabajen en paralelo
	if(rank==MASTER){
		for(int dest=1; dest<size; dest++){
			int rowoffset = rankrows*(dest-1);
			if(dest==size-1 && matrows%(size-1)!=0){
				rankrows=matrows%(size-1)+(matrows/(size-1));
			}
			if(dest<=matrows){
				for(int i=0; i<rankrows; i++){
					MPI_Send(&mat[i+rowoffset][0], matrows, MPI_INT, dest, SEND_ROW, MPI_COMM_WORLD);
				}	
			}
		}
	}	
	
	//Los cores reciben sus respectivas filas de la matriz
	else if(rank<=matrows){
		if(matrows%(size-1)!=0 && rank==size-1){
			rankrows=matrows%(size-1)+(matrows/(size-1));
		}
		mat2 = (int**)malloc(rankrows * sizeof(int*));
		for (int i=0;i<rankrows;i++){
			mat2[i] = (int*)malloc(col * sizeof(int));
		}
		for(int i=0; i<rankrows; i++){
			MPI_Recv(&mat2[i][0], matrows, MPI_INT, MASTER, SEND_ROW, MPI_COMM_WORLD, &status);
		}
	}
	
	//Master imprime la matriz y el vector
	if(rank == MASTER){   
		rankrows = matrows/(size-1);
		printmat(mat, matrows, col);
		printf("\n");
		printvec(vec, matrows);
		printf("\n");	
	}
	
	//Los cores multiplican sus respectivas filas por el vector y
	//envían su resultado a Master
	else if(rank<=matrows){
		if(matrows%(size-1)!=0 && rank==size-1){
			rankrows=matrows%(size-1)+(matrows/(size-1));
		}
		int* subresult = (int*)malloc(rankrows * sizeof(int));
		for(int i=0; i<rankrows; i++){
			subresult[i]=0;
		}
		for(int i=0; i<rankrows; i++){
			for (int j = 0; j < col; j++){
				subresult[i]+=mat2[i][j]*vec[j];
			}
		}
		MPI_Send(&subresult[0], rankrows, MPI_INT, MASTER, RECEIVE_RES, MPI_COMM_WORLD);
	}
	
	//Master recibe los resultados, los ordena, imprime el vector 
	//resultante y concluye con su ejecución
	if(rank==MASTER){
		int* recvresult = (int*)malloc(rankrows * sizeof(int));
		int limit;

		for(int sender=1; sender<size; sender++){
			int rowoffset = rankrows*(sender-1);
			if(sender==size-1 && matrows%(size-1)!=0){
				rankrows=matrows%(size-1)+(matrows/(size-1));
			}
			if(size>matrows){
				rankrows=1;
			}
			if(sender<=matrows){
				MPI_Recv(&recvresult[0], rankrows, MPI_INT, sender, RECEIVE_RES, MPI_COMM_WORLD, &status);
				for(int i = 0; i < rankrows; i++){
					vecresult[i+rowoffset]=recvresult[i];
				}
			}
		}
		printvec(vecresult, col);
	}
	
	MPI_Finalize();
	return 0;
}
