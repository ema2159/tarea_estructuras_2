
#define randsize 5
//Matrix functions
void freemat(int** mat, int matrows) {
	for (int i=0;i<matrows;i++){
		free(mat[i]);
	}
	free(mat);
}

void printmat(int** mat, int matrows, int matcols) {
	for (int i=0;i < matrows;i++){
		for (int j = 0; j < matcols; j++){
			printf("%d\t", mat[i][j]);
		}
	printf("\n");
	}
}

void matrandomize(int** mat, int matrows, int matcols) {
	for (int i=0;i < matrows;i++){
		for (int j = 0; j < matcols; j++){
			mat[i][j] = rand()%randsize;
		}
	}
}

//Vector Functions
void printvec(int* mat, int vecsize) {
	for (int i = 0; i < vecsize; i++){
		printf("%d\t", mat[i]);
	}
	printf("\n");
}

void vecrandomize(int* mat, int vecsize) {
	for (int i = 0; i < vecsize; i++){
		mat[i] = rand()%randsize;
	}
}

//Matrix x Vector
int* vecXmat(int** mat, int*vec, int matrows, int matcols){
	int* vecresult = (int*)malloc(matcols * sizeof(int));
	for (int i = 0; i < matcols ; i++){
			vecresult[i]+=0;
		}
	for (int j = 0; j < matrows; j++){
		for (int i = 0; i < matcols ; i++){
			vecresult[j]+=vec[i]*mat[j][i];
		}
	}
	return vecresult;
}
