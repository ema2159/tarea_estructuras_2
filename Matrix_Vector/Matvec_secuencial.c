
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "functions.c"


//Defines
#define MASTER 0 
#define FROM_WORKER 2    
#define SEND_ROW 1     
#define SEND_VEC 2
#define RECEIVE_RES 3

int main(int argc, char **argv)
{	//Random number generator
	srand(time(NULL)); 
	//Variables setting
	int matrows = strtol(argv[1], NULL, 10);
	int col = matrows;
	int rank, size, dest;
	int* vec = (int*)malloc(matrows * sizeof(int));
	int* row = (int*)malloc(matrows * sizeof(int));
	int* vecresult = (int*)malloc(matrows * sizeof(int));
	int** mat = (int**)malloc(matrows * sizeof(int*));
	for (int i=0;i<matrows;i++){
		mat[i] = (int*)malloc(col * sizeof(int));
	}
	
	matrandomize(mat, matrows, col);
	vecrandomize(vec, matrows);
	printf("Matriz: \n");
	printmat(mat, matrows, col);
	printf("Vector: \n");
	printvec(vec, matrows);
	printf("Resultado: \n");
	vecresult = vecXmat(mat, vec, matrows, col);
	printvec(vecresult, matrows);
	
	return 0;
}
