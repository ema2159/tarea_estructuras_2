TAREA 2
==========
IE0521 Estructuras de computadoras digitales II
--------------------
Se presenta la solución a 3 problemas de forma secuencial y paralela usando C y OpenMPI.

+ 1) Multiplicación de matrices y vectores
+ 2) Ruta más corta entre ciudades
+ 3) Generación de números primos

Integrantes:
--------------------
+ Dunia Barahona
+ Emmanuel Bustos